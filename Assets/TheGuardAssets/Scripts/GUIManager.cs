﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

	//keep public
	public Sprite stdCrossair;


	//set private?
	public Image currentCrossairIMG;
	public Image handIMG;
	[Header ("Text canvases")]
	public Canvas textCanvases;
	public Text textCanvasItemNombre;
	public Text textCanvasItemDescripcion;
	// Use this for initialization
	void Start () {
		//currentCrossair = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("MainCrossairCanvas").transform.FindChild("Image").GetComponent<Image>().sprite;
		currentCrossairIMG = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("MainCrossairCanvas").transform.FindChild("Crossair").GetComponent<Image>();
		handIMG = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("MainCrossairCanvas").transform.FindChild("Hand").GetComponent<Image>();
		textCanvases = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("TextCanvases").GetComponent <Canvas>();
		textCanvasItemNombre = textCanvases.gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>();
		textCanvasItemDescripcion = textCanvases.gameObject.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();
	}
	
	public void AA_SetStdCrossair(){
		//currentCrossairIMG.sprite = stdCrossair;
		currentCrossairIMG.enabled = true;
		handIMG.enabled = false;
	}

	public void AA_SetCustomCrossair (Sprite customCrossair){
		
		currentCrossairIMG.enabled = false;//ç
		handIMG.enabled = true;
		handIMG.sprite = customCrossair;
//		currentCrossairIMG.mainTexture = customCrossair;
	}

	public void AA_ShowTextForItem(){
		textCanvases.enabled = true;
	}

	public void AA_HideTextForItem(){
		textCanvases.enabled = true;
	}
}
