﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {


	//set private
	///references to external
	//public GameManager gameManager;
	public InteractionManager interactionManager;
	public InventoryManager inventoryManager;
	// Use this for initialization
	void Start () {
		//gameManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameManager>();
		interactionManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InteractionManager>();
		inventoryManager =GameObject.FindGameObjectWithTag("GameController").GetComponent<InventoryManager>();
	}
	
	//INTERACTION ACTIONS
	/// interact with sceen using bare hand

	public void AA_ExploringMainAction(){
			if (interactionManager.AA_IsItemPointed()){
				interactionManager.AA_ShowInteractionMenu ();
				Debug.Log ("item pointed, call show menu");
			}
			else
				Debug.Log  ("item not pointed. do nothing");
			Debug.Log  ("exploring");
	}

	public void AA_InitConversation (){

	}
	public void AA_UseItemInHandAgainstScene(){
		if (interactionManager.AA_IsItemPointed()){
			interactionManager.AA_UseItemAgainstScene ();

		}
	}


	public bool AA_TargetingConversationAgent(){
		if (interactionManager.currentlyPointedAgent == null)
			return false;
		if (interactionManager.currentlyPointedAgent.GetComponent<ConversationAgent>() == null)
			return false;
				
		return true;
	
	}

	public bool AA_TargetingInteractiveItem(){
		if (interactionManager.currentlyPointedAgent == null)
			return false;
		if (interactionManager.currentlyPointedAgent.GetComponent<InteractionAgent>() == null)
			return false;

		return true;

	}

	//INTERACTION ACTIONS END

	//INVENTORY ACTIONS
	public void AA_ShowInventory (){
		//interactionManager.AA_ShowInventory();
		inventoryManager.AA_ShowInventory();
	}



	public void AA_HideInventory(){
		//interactionManager.AA_HideInventory();
		inventoryManager.AA_HideInventory();
	}
	//INVENTORY ACTIONS END
}
