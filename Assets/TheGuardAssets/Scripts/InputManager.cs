﻿ using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {


	//set private
	//-references to external
	public PlayerManager playerManager;
	public GameManager gameManager;
	public InventoryManager inventoryManager;
	public InteractionManager interactionManager;
	// Use this for initialization
	void Start () {
		playerManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<PlayerManager>();
		gameManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameManager>();
		inventoryManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<InventoryManager>();
		interactionManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<InteractionManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.anyKeyDown){
			Debug.Log ("process key");
  			switch (gameManager.gameStatus){
				case GameManager.GameStatus.Exploring:
				if (Input.GetMouseButtonDown(0) && inventoryManager.handStatus == InventoryManager.HandStatus.HandFree && playerManager.AA_TargetingInteractiveItem()){ //main action (attempt to interact with item item)
						playerManager.AA_ExploringMainAction();
					}

				if (Input.GetMouseButtonDown(0)  && inventoryManager.handStatus == InventoryManager.HandStatus.HandFree && playerManager.AA_TargetingConversationAgent()){//esto es 'inicio una conversacion'
					playerManager.AA_InitConversation();
				}

				if (Input.GetMouseButtonDown(0) && inventoryManager.handStatus == InventoryManager.HandStatus.ItemInHand){
					//Debug.Log ("TRY TO USE ITEM IN HAND WITH ITEM MARKED BY INTERACTION TOOL IF ANY");ç
					playerManager.AA_UseItemInHandAgainstScene();
				}


				if (Input.GetKeyDown (KeyCode.I)){        //en este caso abrir inventario
					playerManager.AA_ShowInventory();
				}  

				//		playerManager.AA_OpenInventory();	
				break;

				case GameManager.GameStatus.InInventory:
					if (Input.GetKeyDown (KeyCode.I)){        //en este caso abrir inventario
						playerManager.AA_HideInventory();
					} 
				//if boton izq raton (aunque ojo los clicks on object habra que ver si conviene mas hacerlo desde aqui o desde el proipo onmousedonw desde el object)
				//if boton der raton  (aunque ojo los clicks on object habra que ver si conviene mas hacerlo desde aqui o desde el proipo onmousedonw desde el object)
				//if tal-key-pressed (para el seleccionar objetos con el mando por ejemplo)
				break;

			}  

		

			switch (inventoryManager.handStatus){
			case InventoryManager.HandStatus.ItemInHand:
				if (Input.GetMouseButton(1)){
					inventoryManager.AA_PutItemOutFromHand();
				}
				break;
			}
				
/*
			case GameManager.GameStatus.ItemInHand:
				if (Input.GetMouseButton(1)){
					inventoryManager.AA_PutItemOutFromHand();
				}
				break;
*/
			
		}
	}
}
