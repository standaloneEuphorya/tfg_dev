﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
public class InteractionManager : MonoBehaviour {
	//for debug only


	//keep public
	public InteractiveElementAgent currentlyPointedAgent = null;


	//set private

	public Camera uiCamera = null;
	public GameManager gameManager;
	public InventoryManager inventoryManager;
	public GameObject interactionMenuGO = null;
	public GameObject inventoryMenuGO = null;

	//--FROM PLAYER ELEMENTS
	public RigidbodyFirstPersonController fpsController;
	public Rigidbody fpsRigidBody;
	public void Start (){	
		fpsController = GameObject.FindGameObjectWithTag("Player").GetComponent<RigidbodyFirstPersonController>();
		uiCamera = GameObject.FindGameObjectWithTag("MainCamera").transform.FindChild("UI Camera").GetComponent<Camera>();
		interactionMenuGO = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("CanvasPROTOTYPE1").gameObject;
		inventoryMenuGO = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("InventoryCanvas").gameObject;
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		inventoryManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InventoryManager>();
		fpsRigidBody = fpsController.gameObject.GetComponent<Rigidbody>();
		//init vars
		//uiCamera.enabled = false;
	}

	public void Update (){
		
	}

	//-->TO BE CALLED FROM EXTERNAL scripts

	public void AA_ShowInteractionMenu(){
		Cursor.visible = true;
		fpsController.enabled = false;
		//uiCamera.enabled = true;

		//interactionMenuGO.transform.LookAt(fpsController.transform);
		//interactionMenuGO.transform.Rotate (0,180,0);
		//interactionMenuGO.transform.Rotate (0,180,0);


		PutMenuInPlaceHitBased(interactionMenuGO);
		interactionMenuGO.transform.LookAt(uiCamera.transform);
		interactionMenuGO.GetComponent<Canvas>().enabled = true;
		//interactionMenuGO.transform.LookAt(uiCamera.transform);




		gameManager.gameStatus = GameManager.GameStatus.InInteractionMenu;

		Debug.Log ("ORIENTANDO MENU PROTOTYPE");

	}

	public void AA_UseItemAgainstScene (){
		//currentlyPointedAgent.iSM.AA_TryToAdvance (inventoryManager.itemInHand);
		currentlyPointedAgent.AA_InteractWith (inventoryManager.itemInHand);
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");
		Debug.Log ("TRY THIS");


		
		//ima.AA_TryToAdvance (itemInHand);
	}
/*
	public void AA_ShowInventory (){
		Cursor.visible = true;
		//fpsRigidBody.isKinematic = true;
		//uiCamera.enabled = true;
		fpsController.enabled = false;
		PutMenuInPlaceFpcForwardBased (inventoryMenuGO);
		inventoryMenuGO.GetComponent<Canvas>().enabled = true;
		//PutMenuInPlaceRawForwardBased (inventoryMenuGO);
		gameManager.gameStatus = GameManager.GameStatus.Inventory;
	}

	public void AA_HideInventory (){
		Cursor.visible = false;
		//fpsRigidBody.isKinematic = false;
		fpsController.enabled = true;
		inventoryMenuGO.GetComponent<Canvas>().enabled = false;
		//uiCamera.enabled = false;
		//PutMenuInPlace (inventoryMenuGO);
		gameManager.gameStatus = GameManager.GameStatus.Exploring;
	}
*/

	//public to be called from other scripts
	public bool AA_IsItemPointed(){
		if (currentlyPointedAgent != null)
			return true;

		return false;
	}


	//-------->TO BE CALLED FROM interaction menu
	//-------->TO BE CALLED FROM interaction menu
	//-------->TO BE CALLED FROM interaction menu
	//-------->TO BE CALLED FROM interaction menu
	public void AA_HideInteractionMenu(){
		Cursor.visible = false;
		fpsController.enabled = true;
		//uiCamera.enabled = false;
		interactionMenuGO.GetComponent<Canvas>().enabled = false;
		gameManager.gameStatus = GameManager.GameStatus.Exploring;
	}

	public void AA_PickObjectFromScene (){
		//if(currentlyPointedAgent.iSM.AA_TryToAdvance(ItemTransition.TransitionType.buttonPick))
		if(currentlyPointedAgent.AA_InteractWith(ItemTransition.TransitionType.buttonPick))
		{			

			//inventoryManager.AA_AddItemToInventory (currentlyPointedAgent.iSM);

			AA_HideInteractionMenu();
		}


	}

	public void AA_UseItemInScene (){
		//if(currentlyPointedAgent.iSM.AA_TryToAdvance(ItemTransition.TransitionType.buttonUse))
		if(currentlyPointedAgent.AA_InteractWith(ItemTransition.TransitionType.buttonUse))
		{			

			//inventoryManager.AA_AddItemToInventory (currentlyPointedAgent.iSM);

			AA_HideInteractionMenu();
		}


	}




	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
/*
	private void PutMenuInPlaceFpcForwardBased (GameObject menuToSet){
		Vector3 newPosition =  uiCamera.transform.position + uiCamera.gameObject.transform.forward * 5;
		menuToSet.transform.position = newPosition;
	}
*/
	private void PutMenuInPlaceHitBased (GameObject menuToSet){
		//trazo un rayo desd ela uiCamera
		RaycastHit hit;

		Debug.DrawRay (uiCamera.transform.position, uiCamera.transform.forward*100, Color.red,20);

		Physics.Raycast (uiCamera.transform.position,     uiCamera.transform.forward    , out hit, 100);

		if (hit.transform != null && hit.transform.tag == "InteractiveByRay"){
			Vector3 newPosition =  uiCamera.transform.position + uiCamera.gameObject.transform.forward * 5;
			menuToSet.transform.position = newPosition;
			//bloquear controller
		} else{
				//Debug.Log ("CAN'T put menu");  

		}
		//veo dodne colisiona
		//pongo el menu ahi
		//corregir la posicion (mejor si no es necesario)
	}

}
//at some point desde aqui se llamara  al currentlyPointedAgent.ItemStateManagement.AA_TryToAdvance (currentCursor)
//														 "             "           .AA_TryToAdvance (botonPulsado)