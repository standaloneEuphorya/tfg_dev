﻿using UnityEngine;
using System.Collections;

public class InteractionAgent : InteractiveElementAgent {

	//to set private
	public ItemManagementAgent iSM;
	// Use this for initialization
	void Start () {
		try {
			iSM = transform.parent.GetComponent<ItemManagementAgent>();
			} catch {

			}
	}
	
	public override bool AA_InteractWith (ItemManagementAgent transitionType){
		return iSM.AA_TryToAdvance (transitionType);
	}

	public override bool AA_InteractWith (ItemTransition.TransitionType triggerType){
		return iSM.AA_TryToAdvance (triggerType);
	}

}
