﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class InventoryManager : MonoBehaviour {

	//basicamente:
	//--la lista de objetos 'en inventario'
	//--el mostrarlos, ordenarlos y tal
	//--las funciones 'mover a inventario', 'coger de inventario' y tal.. ¿aqui o en interaccion? (desde interaccion se tendran que llamar, claro... PickObject por ejemplo tendra que llamarla asi que tendra que haber un apuntador de interaction manager a inventory manager
	//--las funciones 'mostrar' y 'ocultar' inventario


	//still undediced
	public enum HandStatus{ItemInHand, HandFree}

	public HandStatus handStatus = HandStatus.HandFree;
	//keep public
	public GameObject instantiableButton;


	//keep public (for GUI changes during inventory use)
	public Texture2D stdMousePointer;




	//set private (or label as debug)
	public List <ItemManagementAgent> inventoryList = new List<ItemManagementAgent>();
	//--referencias al item del inventario seleccionado para interactuar
	public Button currentButton;
	public ItemManagementAgent currentIMA;
	public ItemManagementAgent itemInHand = null;

	//-----------
	//set private
	public GameObject inventoryBackground;
	public Canvas interactionInventory;
	public RigidbodyFirstPersonController fpsController;
	public GameObject inventoryMenuGO = null;
	public GameObject conversationMenuGO = null;
	public GameManager gameManager;
	public Camera uiCamera;
	public GUIManager guiManager;
	public void Start (){
		Cursor.SetCursor(stdMousePointer, Vector2.zero, CursorMode.Auto);
		Cursor.visible = false;
		interactionInventory = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("InteractionInventory").gameObject.GetComponent<Canvas>();
		//inventoryBackground =(GameObject) GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("InventoryCanvas").transform.FindChild("InventoryBackground");
		inventoryBackground = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("InventoryCanvas").gameObject.transform.FindChild ("InventoryBackground").gameObject;
		fpsController = GameObject.FindGameObjectWithTag("Player").GetComponent<RigidbodyFirstPersonController>();
		inventoryMenuGO = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("InventoryCanvas").gameObject;
		conversationMenuGO = GameObject.FindGameObjectWithTag("WSCanvasHolder").transform.FindChild("ConversationCanvas").gameObject;
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		uiCamera = GameObject.FindGameObjectWithTag("MainCamera").transform.FindChild("UI Camera").GetComponent<Camera>();
		guiManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GUIManager>();
	}


	//TO BE CALLED FROM EXTERNAL
	//TO BE CALLED FROM EXTERNAL
	//TO BE CALLED FROM EXTERNAL

	public void AA_AddItemToInventory (ItemManagementAgent itemToAdd){
		inventoryList.Add (itemToAdd);
		 AA_CreateNewButton (itemToAdd);
	}



	public void AA_SelectItemToUse(){

		//esto saca el menu de 'usar desde el inventario' que basicamente es 'coger' y 'examinar'. ver si se puede llamar desde el player manager en lugar
		//de directamente para mantener la coherencia
		interactionInventory.enabled = true;
		RectTransform rtMenu = interactionInventory.GetComponent<RectTransform>();
		rtMenu.position = currentButton.GetComponent<RectTransform>().position;
		rtMenu.rotation = currentButton.GetComponent<RectTransform>().rotation;
		interactionInventory.enabled = true;
		Debug.Log ("item selected to use from INVENTORYMANAGER");
		Debug.Log ("item :"+ rtMenu.rotation);
		Debug.Log ("button :"+ currentButton.GetComponent<RectTransform>().rotation);
	}


	//TO BE CALLED FROM INTERACTION MANAGER
	//TO BE CALLED FROM INTERACTION MANAGER
	//TO BE CALLED FROM INTERACTION MANAGER

	public void AA_HideInventory (){
		Cursor.visible = false;
		//fpsRigidBody.isKinematic = false;
		fpsController.enabled = true;
		inventoryMenuGO.GetComponent<Canvas>().enabled = false;
		//uiCamera.enabled = false;
		//PutMenuInPlace (inventoryMenuGO);
		gameManager.gameStatus = GameManager.GameStatus.Exploring;
	}


	public void AA_ShowInventory (){
		Cursor.visible = true;
		//fpsRigidBody.isKinematic = true;
		//uiCamera.enabled = true;
		fpsController.enabled = false;
		PutMenuInPlaceFpcForwardBased (inventoryMenuGO);

		//inventoryMenuGO.transform.LookAt (fpsController.transform);
		//Vector3 auxV = fpsController.transform.position - inventoryMenuGO.transform.position;
		//inventoryMenuGO.transform.LookAt(fpsController.transform.position - auxV);
		inventoryMenuGO.transform.LookAt(uiCamera.transform);
		inventoryMenuGO.transform.Rotate (0,180,0);


		inventoryMenuGO.GetComponent<Canvas>().enabled = true;
		//PutMenuInPlaceRawForwardBased (inventoryMenuGO);
		gameManager.gameStatus = GameManager.GameStatus.InInventory;
	}

	public void AA_ShowConversationCanvas() {
		Cursor.visible = true;
		fpsController.enabled = false;
		PutMenuInPlaceFpcForwardBased (conversationMenuGO);
		conversationMenuGO.transform.LookAt (uiCamera.transform);
		conversationMenuGO.transform.Rotate (0,180,0);
	}


	public void AA_PutItemOutFromHand(){
		
		itemInHand = null;
		Cursor.SetCursor(stdMousePointer, Vector2.zero, CursorMode.Auto);
		if (currentButton != null)
			currentButton.interactable = true;
		
		//gameManager.gameStatus = GameManager.GameStatus.InInventory;
		handStatus = HandStatus.HandFree;
		guiManager.AA_SetStdCrossair();
		currentButton = null;
		currentIMA = null;
	}


	//TO BE CALLED FROM GUIs
	//TO BE CALLED FROM GUIs
	//TO BE CALLED FROM GUIs

	//--esto es 'al pulsar el boton 'coger' en el menu de interaccion en inventario' (step one es 'cogo el item y lo pongo como cursor'
	public void AA_TakeItemInHand (){

		Cursor.visible = true;
		itemInHand = currentIMA;

		Cursor.SetCursor(itemInHand.item2D.GetComponent<INA_2dItem>().cursor, Vector2.zero, CursorMode.Auto);
		guiManager.AA_SetCustomCrossair ( itemInHand.item2D.GetComponent<INA_2dItem>().inventory); //chage crossair texture
		interactionInventory.GetComponent<Canvas>().enabled = false;
		currentButton.interactable = false;
		//gameManager.gameStatus = GameManager.GameStatus.ItemInHand;
		handStatus = HandStatus.ItemInHand;

//		Cursor.SetCursor(itemInHand.item2D.GetComponent<INA_2dItem>().cursor, Vector2.zero, CursorMode.Auto);
	}

	public void AA_CloseInteractionInventoryMenu(){
		interactionInventory.enabled = false;
		currentButton = null;
		currentIMA = null;
	}

	public void AA_ClickOnItem (Button itemClicked, ItemManagementAgent ima){
		if (currentIMA == null){
			AA_PutItemInHand ( itemClicked,  ima);
			Debug.Log ("PASA CLICKONITEM");
		} else {
			Debug.Log (" y ahora avanzo por el automata item 2"+ itemClicked.transform.name);
			ima.AA_TryToAdvance (itemInHand);
		}			
	}


	public void AA_ShowItemText (ItemManagementAgent ima){
		guiManager.textCanvasItemNombre.text = ima.currentState.stateName;
		guiManager.textCanvasItemDescripcion.text = ima.currentState.stateDescription;
		guiManager.textCanvases.enabled = true;
	}

	public void AA_HideItemText (){
		guiManager.textCanvasItemNombre.text = "XXXX";
		guiManager.textCanvasItemDescripcion.text = "XXXX";
		guiManager.textCanvases.enabled = false;
	}






	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	//*************iINTERNAL SUPPORT
	private void AA_PutItemInHand (Button itemClicked, ItemManagementAgent ima){
		currentButton = itemClicked ;
		currentIMA = ima;
		AA_SelectItemToUse();
	}


	private void PutMenuInPlaceFpcForwardBased (GameObject menuToSet){
		Vector3 newPosition =  uiCamera.transform.position + uiCamera.gameObject.transform.forward * 5;
		menuToSet.transform.position = newPosition;
	}

	private void AA_CreateNewButton(ItemManagementAgent itemToAdd){
		GameObject itemInInventory = (GameObject) Instantiate (instantiableButton);
		itemInInventory.GetComponent<Image>().sprite =   itemToAdd.item2D.GetComponent<INA_2dItem>().inventory;
		itemInInventory.transform.SetParent(inventoryBackground.transform);
		itemInInventory.GetComponent<ButtonInventoryAgent>().ima = itemToAdd;



		//RectTransform rt = inventoryBackground.GetComponent<RectTransform>();
		RectTransform rt = itemInInventory.GetComponent<RectTransform>();

		//rt.position = new Vector3 (0,0,0);
		rt.localPosition = new Vector3 (0,0,0);
		//rt.rotation.eulerAngles = new Quaternion (0,0,0,0);
		rt.localScale = new Vector3 (1,1,1);
		rt.localRotation = Quaternion.identity;
//		RectTransform r = new RectTransform();
/*
		r.position = Vector3.zero;
//		r.rotation.eulerAngles = Vector3.zero;
		r.localScale = new Vector3(1,1,1);

		inventoryBackground.GetComponent<RectTransform>() = r;
*/


		itemToAdd.buttonWhenInInventory =  itemInInventory;
		//Debug.Log ("inventoriItemInstantiated<<<-------");
	}

}


