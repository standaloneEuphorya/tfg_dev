﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ItemState : MonoBehaviour {
	//public enum StateType{Standard, KillObject};	
	public enum ObjectLocation {Scene,EntryInventory, Inventory,Killed}  //entry inventory funciona como punto de entrada al inventario. Al llegar aqui se instancia el boton


	[Header("Setup Area")]
	[Tooltip("Text to show on GUI when enter the state")]
	public string stateName;
	public string stateDescription;

	public string onEnterText;
	public AudioClip onEnterSound;
	public string onEnterAnimationBoolTrigger;

	public ObjectLocation objectLocation = ObjectLocation.Scene;
	public float endOfTransitionDelay = 0;   //--->> tiempo en segundos que el estado se espera antes de ejecutar las 'lateEntryActions' para permitir que animaciones y sonidos terminen de reproducirse
	public float endOfTransitionTimer = 0;
	public bool waitingForEndOfTransition = false;
	[Header("Inner parameters")]
	//[Tooltip("Text to show on GUI when enter the state")]
	//public StateType stateType = StateType.Standard;


	//to set private
	public InventoryManager inventoryManager;
	public ItemManagementAgent ima;
	public Animator animator;

	[Header("Debug parameters")]
	public string descripcion = "describe state here";

	void Start () {
		inventoryManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InventoryManager>();
		ima = gameObject.transform.parent.parent.GetComponent<ItemManagementAgent>();
		animator = ima.item3D.GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	

		//triggering late actions
		if (waitingForEndOfTransition)
		{
			endOfTransitionTimer += Time.deltaTime;
			if (endOfTransitionTimer>= endOfTransitionDelay){				
				AA_LateEntryActions();
				Debug.Log (" LATE ACTION ");
				waitingForEndOfTransition = false;
				endOfTransitionTimer = 0;

			}
		}

	}



	//TO BE CALLED FROM EXTERNAL
	public void AA_PerformEntryActions (){
		//incio
		Debug.Log ("performing entry actions");
		//cuerpo
		if (onEnterSound != null){
			ima.item3D.GetComponent<AudioSource>().clip = onEnterSound;
			ima.item3D.GetComponent<AudioSource>().Play();
		}

		if (animator != null && onEnterAnimationBoolTrigger != ""){
			animator.SetBool(onEnterAnimationBoolTrigger, true);

		}
		//fin
		waitingForEndOfTransition = true;  //trigger for late entry actions

	}



	//SUPPORT
	//SUPPORT
	//SUPPORT
	//las transiciones podria ser interesante meterlas en una lista variable local 'on start'
	public List <ItemTransition> AA_GetTransitions(){
		List <ItemTransition> transitions = new List<ItemTransition>();
		ItemTransition [] transitionsArray;

		transitionsArray = gameObject.GetComponentsInChildren <ItemTransition>();

		foreach (ItemTransition item in transitionsArray)
			transitions.Add (item);


		return transitions;
	}



	//cosas como desactivar el objeto
	/// //actiones performed after endOfTransitionDelay timer
	private void AA_LateEntryActions(){
		

		switch (objectLocation){
		case ObjectLocation.EntryInventory:
			inventoryManager.AA_AddItemToInventory (ima);
			AA_Deactivate3dItemKeepingGO();
			//ima.item3D.SetActive (false);
			break;

		case ObjectLocation.Killed:
			AA_Deactivate3dItemKeepingGO();
			//inventoryManager.AA_AddItemToInventory (ima);
			//ima.item3D.SetActive (false);
			break;
		}


	}


	private void AA_Deactivate3dItemKeepingGO(){
		ima.item3D.GetComponent<Collider>().enabled= false;
		ima.item3D.GetComponent<MeshRenderer>().enabled = false;
		ima.item3D.GetComponent<InteractionAgent>().enabled = false;
	}


}
