﻿using UnityEngine;
using System.Collections;

public class ItemTransition : MonoBehaviour {
	public enum TransitionType{standard, buttonUse, buttonPick};




	//keep public (setup vars
	[Header("Set up area")]
	public TransitionType transitionType; 
	public ItemManagementAgent transitionActivator;
	public ItemState archTarget;
	public ItemState [] archSecondarytargets;
	public string text; //to override state text if needed
	//for debug purposes
	[Header("Debug area")]
	public string description;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
/*////////////debug
		if (archTarget != null)
			Debug.Log (	archTarget.gameObject.name);
		*/
	}


	public void AA_ProcessAllSecondaryTransitions(){
		//para cada transicion secundaria:
		////actualizar el estado del automata target
		////comprobar el tipo del nuevo estado y si es killer matar al objeto
	}
}
