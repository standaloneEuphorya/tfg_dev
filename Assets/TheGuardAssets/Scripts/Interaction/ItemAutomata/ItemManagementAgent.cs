﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ItemManagementAgent : MonoBehaviour {
	public  enum layer {main, inventory, hidden}; 

	//Keep public
	[Header("Completar desde el Inspector")]
	public GameObject item3D;
	public GameObject item2D;




	//to set privatef
	[Header("Inner use (set private)")]
	public GameObject buttonWhenInInventory;
	public ItemState currentState;
	public List<ItemTransition> currentTransitions;// = new List<ItemTransition>();
	public InventoryManager inventoryManager;

	// Use this for initialization
	void Start () {
		Debug.Log ("cargando lista de transiciones");
		inventoryManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<InventoryManager>();
		if (currentState != null)
			currentTransitions = currentState.AA_GetTransitions();
	}

	//this is for advance when one object is used by other
	public bool AA_TryToAdvance (ItemManagementAgent trigger){
		
		foreach (ItemTransition item in currentTransitions){
			if (item.transitionActivator == trigger){
				/*
				currentState = item.archTarget;
				currentState.AA_PerformEntryActions();
				currentTransitions = currentState.AA_GetTransitions();
				*/
				AA_PerformAdvance (item);
				return true;

			}
		}

		return false;
	}

	public bool AA_TryToAdvance (ItemTransition.TransitionType triggerType){

		//1-- buscar arco tipo trigger
		///si hay, avanzo (y muestro mensajes, proceso animaciones y lo que sea)
		///si no hay 'muestro mensaje de 'accion imposible''

		foreach (ItemTransition item in currentTransitions){
			if (item.transitionType == triggerType){
				/*
				currentState = item.archTarget;
				currentState.AA_PerformEntryActions();
				currentTransitions = currentState.AA_GetTransitions();
				*/
				AA_PerformAdvance (item);

				return true;

			}


		}

		return false;
		/*
		switch (triggerType) {
			case (ItemTransition.TransitionType.buttonPick):
				Debug.Log("transition PICK");
				break;

		}
		*/
	}



	//INNER USE ONLY
	//INNER USE ONLY
	//INNER USE ONLY
	private void AA_PerformAdvance (ItemTransition transition){
		//main transition
		currentState = transition.archTarget;
		currentState.AA_PerformEntryActions();

		currentTransitions = currentState.AA_GetTransitions();

		//secondary transitions (no perform entry actions, just change and kill if needed)

		foreach (ItemState item in transition.archSecondarytargets){
			ItemManagementAgent secondaryItem = item.transform.GetComponentInParent<ItemManagementAgent>();
			secondaryItem.currentState = item;
			secondaryItem.currentState.AA_GetTransitions();
			secondaryItem.AA_EndOfTransition();
			Debug.Log (secondaryItem);

		}

	}

	//for primary and secondary
	private void AA_EndOfTransition (){
		switch (currentState.objectLocation){
			case ItemState.ObjectLocation.Killed:
				inventoryManager.AA_PutItemOutFromHand();
				gameObject.SetActive (false);
				if (buttonWhenInInventory != null)
					GameObject.Destroy (buttonWhenInInventory);
			break;
		}
	}




}
