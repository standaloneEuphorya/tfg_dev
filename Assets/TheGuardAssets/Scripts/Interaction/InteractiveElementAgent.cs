﻿using UnityEngine;
using System.Collections;

public class InteractiveElementAgent : MonoBehaviour {

	public virtual bool AA_InteractWith(ItemManagementAgent transitionType){
		return false;
	}

	public virtual bool AA_InteractWith (ItemTransition.TransitionType triggerType){
		return false;
	}
}
